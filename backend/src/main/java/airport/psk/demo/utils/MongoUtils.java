package airport.psk.demo.utils;

import airport.psk.demo.enums.Availability;
import airport.psk.demo.models.dtoModels.EmployerDTO;
import airport.psk.demo.models.dtoModels.FlightDTO;
import airport.psk.demo.models.flightThings.Crew;
import airport.psk.demo.models.flightThings.MostVisitedPlaces;
import airport.psk.demo.models.flightThings.Plane;
import airport.psk.demo.models.people.AirLinePilot;
import airport.psk.demo.repositories.MostVisitedPlacesRepository;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;

public class MongoUtils {

    private static final String MONGO_DB_HOST_NAME = "localhost";
    private static final String MONGO_DATABASE_NAME = "test";
    private static final int MONGO_DB_PORT = 27017;

    private static final MongoClient client = new MongoClient(MONGO_DB_HOST_NAME, MONGO_DB_PORT);
    private static final MongoDatabase dataBase = client.getDatabase(MONGO_DATABASE_NAME);


    public static void incrementPopularity(String city, MostVisitedPlacesRepository repository) {

        MongoCollection<Document> collection = dataBase.getCollection("mostVisitedPlaces");
        Document query = new Document();
        query.append("cityName", city);
        Document setData = new Document();
        setData.append("popularity", getPopularity(city, repository.findAll()));
        Document update = new Document();
        update.append("$set", setData);

        collection.updateOne(query, update);
    }

    private static int getPopularity(String city, List<MostVisitedPlaces> list) {
        for (MostVisitedPlaces mostVisitedPlaces : list) {
            if (mostVisitedPlaces.getCityName().equals(city)) {
                return mostVisitedPlaces.getPopularity() + 1;
            }
        }
        return 0;
    }

    public static  void updateDateBase(FlightDTO flightDTO){
        List<EmployerDTO> airLinePilot = flightDTO.getCrew().getAirLinePilots();
        List<EmployerDTO> stewardesses = flightDTO.getCrew().getStewardesses();
        Plane plane = flightDTO.getPlane();
        update2(airLinePilot,stewardesses,plane);
    }

    private static void update2(List<EmployerDTO> pilots, List<EmployerDTO> stewardesses, Plane plane){
        MongoCollection<Document> pilotsCollection = dataBase.getCollection("airLinePilot");
        MongoCollection<Document> stewardessCollection = dataBase.getCollection("stewardess");
        MongoCollection<Document> planesCollection = dataBase.getCollection("plane");
        Document query = new Document();

        for (EmployerDTO employerDTO:pilots) {
            ObjectId objectId = new ObjectId(employerDTO.getId());
            query.append("_id", objectId );
            Document setData = new Document();
            setData.append("availability", "UNAVAILABLE");
            Document update = new Document();
            update.append("$set", setData);
            pilotsCollection.updateOne(query, update);
        }

        for (EmployerDTO employerDTO:stewardesses) {
            ObjectId objectId = new ObjectId(employerDTO.getId());
            query.append("_id", objectId );
            Document setData = new Document();
            setData.append("availability", "UNAVAILABLE");
            Document update = new Document();
            update.append("$set", setData);
            stewardessCollection.updateOne(query, update);
        }

            ObjectId objectId = new ObjectId(plane.getId());
            query.append("_id", objectId );
            Document setData = new Document();
            setData.append("availability", "UNAVAILABLE");
            Document update = new Document();
            update.append("$set", setData);
            planesCollection.updateOne(query, update);
        }
    }

