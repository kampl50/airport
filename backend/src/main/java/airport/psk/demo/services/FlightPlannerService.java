package airport.psk.demo.services;


import airport.psk.demo.models.flightThings.AirPort;
import airport.psk.demo.models.flightThings.Flight;
import airport.psk.demo.models.flightThings.MostVisitedPlaces;
import airport.psk.demo.models.flightThings.Plane;
import airport.psk.demo.models.others.weather.Weather;
import airport.psk.demo.models.people.AirLinePilot;
import airport.psk.demo.models.people.Stewardess;
import airport.psk.demo.repositories.*;
import airport.psk.demo.utils.Distance;
import airport.psk.demo.utils.MongoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightPlannerService {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private MostVisitedPlacesRepository mostVisitedPlacesRepository;

    @Autowired
    private AirLinePilotRepository airLinePilotRepository;

    @Autowired
    StewardessRepository stewardessRepository;

    @Autowired
    PlaneRepository planeRepository;

    public void save(Flight flight) {
        flightRepository.save(flight);
    }

    public void delete(String id) {
        flightRepository.deleteById(id);
    }

    public List<Flight> getAllFlights() {
        return flightRepository.findAll();
    }

    public List<AirLinePilot> getAllPilots() {
        return airLinePilotRepository.findAll();
    }

    public List<String> getBestPilots(String city, String planeMark) {
        return AirLinePilot.getBestPilots(city, planeMark, this.airLinePilotRepository);
    }

    public List<Stewardess> getAllStewardesses() {
        return stewardessRepository.findAll();
    }

    public List<String> getStewardessesByCity(String city) {
        return Stewardess.getBestStewardesses(city, this.stewardessRepository);
    }

    public List<Plane> getAllPlanes() {
        return planeRepository.findAll();
    }

    public List<MostVisitedPlaces> getAllMostVisitedPlaces() {
        return mostVisitedPlacesRepository.findAll();
    }

    public Weather getWeather(String city) {
        return Weather.getWeatherFromJSON(city);
    }

    public String bestPlaneToFlight(String city) {
        return Plane.getBestPlaneToFlight(city, planeRepository);
    }

    public List<AirPort> getAirPorts(String cityName) {
        return AirPort.getAirPortsFromJSON(cityName);
    }

    public double getDistance(String city) {
        return Distance.getDistance(city);
    }

    public void increasePopularity(String city) {
        MongoUtils.incrementPopularity(city, mostVisitedPlacesRepository);
    }
}
