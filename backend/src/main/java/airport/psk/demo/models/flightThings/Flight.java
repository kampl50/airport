package airport.psk.demo.models.flightThings;


import airport.psk.demo.models.dtoModels.FlightDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Flight {

    @Id
    private String id;
    private LocalDateTime departureTime;
    private AirPort departurePlace;
    private AirPort arrivalPlace;
    private Crew crew;
    private Plane plane;

    public Flight(LocalDateTime departureTime, AirPort arrivalPlace, Crew crew, Plane plane) {
        AirPort ourAirPort = new AirPort("Warsaw", "WAW", "PL", "Warsaw Chopin", 52.16, 20.96);
        this.departureTime = departureTime;
        this.departurePlace = ourAirPort;
        this.arrivalPlace = arrivalPlace;
        this.crew = crew;
        this.plane = plane;
    }

    public static Flight convertFromFlightDTO(FlightDTO flightDTO) {
        return new Flight(flightDTO.getDepartureTime(),
                flightDTO.getArrivalPlace(),
                flightDTO.getCrew(),
                flightDTO.getPlane());
    }
}
