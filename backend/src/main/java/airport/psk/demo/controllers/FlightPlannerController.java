package airport.psk.demo.controllers;


import airport.psk.demo.models.dtoModels.EmployerDTO;
import airport.psk.demo.models.dtoModels.FlightDTO;
import airport.psk.demo.models.flightThings.*;
import airport.psk.demo.models.others.weather.Weather;
import airport.psk.demo.models.people.AirLinePilot;
import airport.psk.demo.models.people.Stewardess;
import airport.psk.demo.services.FlightPlannerService;
import airport.psk.demo.utils.MongoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class FlightPlannerController {

    @Autowired
    private FlightPlannerService flightPlannerService;


    @PostMapping("/createFlight")
    public void createFlight(@RequestBody FlightDTO flightDTO) {

        Flight flight = Flight.convertFromFlightDTO(flightDTO);
        flightPlannerService.save(flight);
        flightPlannerService.increasePopularity(flightDTO.getArrivalPlace().getCity());
        MongoUtils.updateDateBase(flightDTO);
    }

    @GetMapping("/distance")
    public double getDistanceBetweenCities(@RequestParam String city) {
        return flightPlannerService.getDistance(city);
    }

    @GetMapping("/bestPlane")
    public String getBestPlane(@RequestParam String city) {
        return flightPlannerService.bestPlaneToFlight(city);
    }

    @GetMapping("/weather")
    public Weather getWeather(@RequestParam String city) {
        return flightPlannerService.getWeather(city);
    }

    @GetMapping("/airPorts")
    public List<AirPort> getAirPorts(@RequestParam String city) {
        return flightPlannerService.getAirPorts(city);
    }

    @GetMapping("/flights")
    public List<Flight> getFlights() {
        return flightPlannerService.getAllFlights();
    }

    @GetMapping("/pilots")
    public List<AirLinePilot> allPilots() {
        return flightPlannerService.getAllPilots();
    }

    @GetMapping("/bestPilots")
    public List<String> getPilotsByCityAndPlaneMark(@RequestParam String city, @RequestParam String planeMark) {
        return flightPlannerService.getBestPilots(city, planeMark);
    }

    @GetMapping("/planes")
    public List<Plane> allPlanes() {
        return flightPlannerService.getAllPlanes();
    }

    @GetMapping("/stewardesses")
    public List<Stewardess> allStewardesses() {
        return flightPlannerService.getAllStewardesses();
    }

    @GetMapping("/stewardessesByCity")
    public List<String> getStewardessesByCity(@RequestParam String city) {
        return flightPlannerService.getStewardessesByCity(city);
    }

    @GetMapping("/mostVisitedPlaces")
    public List<MostVisitedPlaces> allMostVisitedPlaces() {
        return flightPlannerService.getAllMostVisitedPlaces();
    }

    @GetMapping("/test")
    public FlightDTO getfF(@RequestParam String city) {
        AirPort airPort = flightPlannerService.getAirPorts(city).get(0);
        EmployerDTO stewardess = EmployerDTO.convertFromPerson(flightPlannerService.getAllStewardesses().get(0));
        EmployerDTO stewardess2 = EmployerDTO.convertFromPerson(flightPlannerService.getAllStewardesses().get(1));
        EmployerDTO airLinePilot = EmployerDTO.convertFromPerson(flightPlannerService.getAllPilots().get(0));
        EmployerDTO airLinePilot2 = EmployerDTO.convertFromPerson(flightPlannerService.getAllPilots().get(1));
        Plane plane = flightPlannerService.getAllPlanes().get(0);
        List<EmployerDTO> s = List.of(stewardess, stewardess2);
        List<EmployerDTO> a = List.of(airLinePilot, airLinePilot2);
        Crew crew = new Crew(a, s);
        return new FlightDTO(LocalDateTime.now(), airPort, crew, plane);
    }
}
