package airport.psk.demo.controllers;

import airport.psk.demo.models.people.Employer;
import airport.psk.demo.services.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdministratorController {

    @Autowired
    AdministratorService administratorService;

    @GetMapping("/employers")
    public List<Employer> getEmployers() {
        return administratorService.getAllEmployers();
    }

    @PostMapping("/delete")
    public void deleteEmployer(@RequestParam String id) {
        administratorService.deleteEmployer(id);
    }

    @PostMapping("/add")
    public void addEmployer(@RequestBody Employer employer) {
        administratorService.save(employer);
    }
}
